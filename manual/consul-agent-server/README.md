# Установка исполняемого файла

Скачать с официального сайта
https://www.consul.io/downloads

Разархивировать
`unzip consul_1.10.3_linux_amd64.zip`

Скопировать файл в каталог
`cp consul ~/bin/`

Либо если удаленно по scp
`scp consul nngle@158.160.11.199:~/bin`

Добавить путь в переменную $PATH
`export PATH=$PATH:~/bin`

Скопировать файл consul.service в папку /etc/consul.d(предварительно создав папку)
```bash
scp consul.service  nngle@158.160.11.199:~
sudo mv consul.service /etc/systemd/system/
sudo chmod +rwx /etc/systemd/system/consul.service
```
Далее скопировать consul.hcl в /etc/consul.d
```commandline
sudo systemctl daemon-reload
sudo systemctl start consul
```
