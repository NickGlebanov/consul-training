# Конфигурирование consul при помощи Ansible

###

Установить переменную чтобы не проверять ключ(один из методов)`export ANSIBLE_HOST_KEY_CHECKING=False`

### Запустить плейбук

`ansible-playbooks -vvvv -i consul.inv site.yml`

зайти на клиент и проверить что всё ок `consul catalog services`

зайти на ui consul -> http://<IP МАШИНЫ С КЛИЕНТОМ CONSUL>:8500/ui/dc1/services

ссылка на урок https://go.skillbox.ru/profession/profession-dev-ops-pro/devops-engineer-advance/3f289d83-e2d9-4d35-accb-b38583ce229b/videolesson